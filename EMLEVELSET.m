
%the EM LEVELSET function  for initialization and parameter estimation of the Level sets formulation
function v = EMLEVELSET(img,phi)
M = double(img);
[m,n]=size(M);
N=m*n;
% initialisation the statistical parameters corresponding to the pdf pi(�) for the object and background are estimated
X = M(:) ;
K = 2; % % The number of clusters.  : the object and background classes
hp = heaviside(phi); % heaviside : the Heaviside step function 
hn = heaviside(-phi); 
hp=hp(:);
hn=hn(:);
% the mean
mu(1)=sum(hp.*X)./ sum(hp);
mu(2) = sum(hn.*X)./ sum(hn);
mu = mu(:);
%the standard deviation
sig(1)=sum(hp.*((mu(1)-X).^2))./sum(hp);
sig(2)=sum(hn.*((mu(2)-X).^2))./sum(hn);
sigma=[sig(1) ; sig(2)];
sigma = sigma(:);
%the prior probability of pi (�)
pi(1)=sum(hp)./sum(hp+hn);
pi(2)=sum(hn)./sum(hn+hp);

% Calculate the probability for each data point for each distribution.
    
% Matrix to hold the pdf value for each every data point for every cluster.
% One row per data point, one column per cluster.
w = zeros(size(X,1),length(pi)); 

test = 0 ;
iter= 0;
while test~=1 
     iter = iter + 1;
      %%E_step
        for j = 1 : length(pi)
            w(:,j)=pi(j)*mvnpdf(X,mu(j),sigma(j)); %Evaluate the Gaussian for all data points for cluster 'j'.
        end   
    w=w./repmat(sum(w,2),1,size(w,2)); % Divide the weighted probabilities by the sum of weighted probabilities for each cluster.

    %%M_step 
     MM = mu ; %% Store the previous means. 
 % Calculate the prior probability for each cluster 
        pi = sum(w,1)./size(w,1); 
   % Calculate the new mean for cluster 'j' by taking the weighted average of all data points.
    mu = w'*X;
    mu= mu./repmat((sum(w,1))',1,size(mu,2)); 
   % Calculate the standard deviation for cluster 'j' by taking the weighted average of of all data points.  
    for j = 1 : length(pi)
        vari = repmat(w(:,j),1,size(X,2)).*(X- repmat(mu(j,:),size(X,1),1)); 
        sigma(j) = (vari'*vari)/sum(w(:,j),1);  
        % Check for convergence
         if  (abs (MM(1)-mu(1))>10^(-2) && abs(MM(2)-mu(2))>10^-2)
                         test=1;
         end
        
    end
   

   
 end
% Estimation
[c estimate] = max(w,[],2);
v = zeros(size(X,1),1); 

% %Obtain v, which specifies the propagation direction of the evolving front is decided according to the following Bayes� rule
for i=1:N
    if estimate(i)==2
        v(i,1)= -1;
%      
    else
        v(i,1)=1;

   end
end

% 