function phi=initialisation(img)
centre = ginput( 1 )
hold on;
plot( centre(1), centre(2), 'go-' );
hold off;
rayon = round(sqrt(sum(( centre - ginput( 1 ) ).^2 )));

%%%%%%%%%%%%%% Saisi de la taille de l'image %%%%%%%%%%%%%%

tailleimg=size(img);
n = tailleimg( 1 );
m = tailleimg( 2 );
phi = zeros(tailleimg);
for i = 1 : n
  for j = 1 : m
    distance = sqrt( sum( ( centre - [ j, i ] ).^2 ) );
    phi( i, j ) = distance-rayon;
    %%%%%%%% si l'objet se trouve � l'exterieur il faut prendre l'oppos� de phi %%%%%%%%%
    %phi(i,j)=-phi(i,j);
  end
end
