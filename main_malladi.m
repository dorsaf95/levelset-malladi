%%%%%%%%%%%%%%%%%%%%%% main bas� fronti�re: model de Malladi et al. 1995 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;clc;close all;
[filename, pathname ] = uigetfile( '*', 'Veuillez Choisir une image' );
img = imread(strcat(pathname, filename));

if( ndims(img) == 3 )%isrgb( img )
  img = rgb2gray( img );
end
figure(1),title('Image initiale'),imshow(uint8(img));

BW = imbinarize(img); %Binarize 2-D grayscale image or 3-D volume by thresholding
bw = BW(:);

x=10;

phi = malladi_em(img,3000,x);

M = double(img);
[m,n]=size(M);
N=m*n;

X = M(:) ;
esti = zeros(size(X,1),1); 
% vec=uint8(X);
% one = zeros(size(X,1),1); 
% two = zeros(size(X,1),1); 
% three = zeros(size(X,1),1);
p = phi(:);
for i=1:N
    if p(i)<0
        esti(i,1)= 1;
%      
    elseif p(i) > 0
        esti(i,1)=2;
%     else 
%         three(i,1)=128;
% 
   end
end

bc = bw; 
bc = double(bc) ;
for i=1:N
    if bw(i)==1
        bc(i)= 1;
    elseif bw(i)==0
        bc(i) = 2;

   end
end
mc = 0; 
for i=1:N
    if bc(i)~=esti(i)
      mc = mc +1;
   end
end
er = (100 .* mc )./N ;
fprintf('erreur: %f\n',er);

% 
% one = zeros(N,1); 
% two = zeros(N,1); 
% 
% 
% 
% for i=1:N
%     if bc(i)==1
%         one(i,1)= 255;
% %         two(i,1)= 128;
%     else
%         two(i,1)=255;
% %     else
% %        three(i,1)=128; 
% % %         one(i,1)=128;
%     end
%     
% end
% figure;
% mat1 = uint8(vec2mat(one,m));
% mat1=mat1';
% mat2 = uint8(vec2mat(two,m));
% mat2=mat2';
% 
% mat4=zeros(m,n);
% mat3=zeros(m,n);
% image=zeros(m,n,3); 
% image(:,:,1)=mat1; 
% image(:,:,2)=mat3; 
% image(:,:,3)=mat4; 
% subplot(1,4,2);
% imshow(image);
% image2=zeros(m,n,3); 
% image2(:,:,1)=mat3; 
% image2(:,:,2)=mat2; 
% image2(:,:,3)=mat4; 
% subplot(1,4,3);
% imshow(image2);






% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

