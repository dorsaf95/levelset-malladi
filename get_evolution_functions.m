%-- compute curvature along SDF
function [curvature,normGrad,FdotGrad] = get_evolution_functions(phi,feature,idx)

    [dimy, dimx] = size(phi);        
    [y x] = ind2sub([dimy,dimx],idx);  % get subscripts

    %-- get subscripts of neighbors
    ym1 = y-1; xm1 = x-1; yp1 = y+1; xp1 = x+1;

    %-- bounds checking  
    ym1(ym1<1) = 1; xm1(xm1<1) = 1;              
    yp1(yp1>dimy)=dimy; xp1(xp1>dimx) = dimx;    

    %-- get indexes for 8 neighbors
    idup = sub2ind(size(phi),yp1,x);    
    iddn = sub2ind(size(phi),ym1,x);
    idlt = sub2ind(size(phi),y,xm1);
    idrt = sub2ind(size(phi),y,xp1);
    idul = sub2ind(size(phi),yp1,xm1);
    idur = sub2ind(size(phi),yp1,xp1);
    iddl = sub2ind(size(phi),ym1,xm1);
    iddr = sub2ind(size(phi),ym1,xp1);
    
    %-- get central derivatives of SDF at x,y
    phi_x  = (-phi(idlt)+phi(idrt))/2;
    phi_y  = (-phi(iddn)+phi(idup))/2;
    phi_xx = phi(idlt)-2*phi(idx)+phi(idrt);
    phi_yy = phi(iddn)-2*phi(idx)+phi(idup);
    phi_xy = 0.25*phi(iddl)+0.25*phi(idur)...
             -0.25*phi(iddr)-0.25*phi(idul);
    phi_x2 = phi_x.^2;
    phi_y2 = phi_y.^2;
    
    %-- compute curvature (Kappa)
    curvature = ((phi_x2.*phi_yy + phi_y2.*phi_xx - 2*phi_x.*phi_y.*phi_xy)./...
              (phi_x2 + phi_y2 +eps).^(3/2));        

    %-- compute norm of gradient
    phi_xm = phi(idx)-phi(idlt);
    phi_xp = phi(idrt)-phi(idx);
    phi_ym = phi(idx)-phi(iddn);
    phi_yp = phi(idup)-phi(idx);    
    normGrad = sqrt( (max(phi_xm,0)).^2 + (min(phi_xp,0)).^2 + ...
        (max(phi_ym,0)).^2 + (min(phi_yp,0)).^2 );
    
    %-- compute scalar product between the feature image and the gradient of phi
    F_x = 0.5*feature(idrt)-0.5*feature(idlt);
    F_y = 0.5*feature(idup)-0.5*feature(iddn);   
    FdotGrad = (max(F_x,0)).*(phi_xp) + (min(F_x,0)).*(phi_xm) + ...
        (max(F_y,0)).*(phi_yp) + (min(F_y,0)).*(phi_ym);