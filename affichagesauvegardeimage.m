function imagecompose=affichagesauvegardeimage(img,phi)
imagecompose( :, :, 1 ) = img;
imagecompose( :, :, 3 ) = img;
tp=zeros(size(phi));
temp=zeros(size(phi));
[ n, m ] = size( phi );

for i = 2 : n - 1;
  for j = 2 : m - 1;

    % regle de Malladi pour la détection des point du front
    
    ValeurMax = max( max( phi( i:i+1, j:j+1 ) ) );
    ValeurMin = min( min( phi( i:i+1, j:j+1 ) ) );
    tp( i, j ) = ( ( ValeurMax > 0 ) & ( ValeurMin < 0 ) ) | phi( i, j ) == 0;

  end
end
%[c,h] = contour(phi,[0 0],'Linewidth',2);
tp;
temp=img;
temp( find( tp ) ) = 256;
imagecompose( :, :,2) = temp;
% convertir en uint8
imagecompose = uint8( imagecompose );
