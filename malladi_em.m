function phi = malladi_em(img,max_its,x)

    %-- ensures image is 2D double matrix
    img = im2graydouble(img);
  
  
    phi=initialisationlevelset(img);%% initialisation Cercle
%     phi=initialisationRectangle(img,x); %% initialisation Rectangle
    
    
    init_mask = phi<=0;
    
    %-- Compute feature image from gradient information
    h = fspecial('gaussian',[5 5],100);
    feature = imfilter(img,h,'same');
    [FX,FY] = gradient(feature);
    feature = sqrt(FX.^2+FY.^2+eps);
    %feature = 1 ./ ( 1 + feature.^2 );
    feature = exp( -abs( feature ) );
    
    
    %--main loop
    its = 0;      stop = 0;
    prev_mask = init_mask;   c = 0; 
    %Obtain the probability density function of the object and background classes using the EM algorithm.
    v=EMLEVELSET(img,phi); % V : the propagation direction of the evolving front 

    while ((its <= max_its) && ~stop)
        
        

        idx = find(phi <= 1.2 & phi >= -1.2);  %-- get the curve's narrow band
        
        if ~isempty(idx)
            %-- intermediate output
            
                if ( mod(its,50)==0 )
                    disp(its);

                        imagecompose=affichagesauvegardeimage(img,phi);
                        imagecompose = uint8( imagecompose );
                        hold on
                        imshow(imagecompose);drawnow;
                end
            
 
            %-- force from image information
            F = feature(idx);
            [curvature,normGrad,FdotGrad] = ...
                get_evolution_functions(phi,feature,idx);  % force from curvature penalty

            %-- gradient descent to minimize energy
            dphidt1 = F.*curvature.*normGrad;
            dphidt1 = dphidt1./max(abs(dphidt1(:)));
            
            dphidt3 = F.*normGrad;
            dphidt3 = dphidt3./max(abs(dphidt3(:)));
            
            dphidt = dphidt1 + v(idx).*dphidt3; % malladi
            %-- maintain the CFL condition
            %dt = .45/(max(abs(dphidt))+eps);
            dt=0.2;
            %-- evolve the curve; pour les images m�dicales(coeur et
            %brain), il faut mettre un signe n�gatif devant dt
            phi(idx) = phi(idx) + dt.*dphidt;

            %-- Keep SDF smooth
            phi = sussman(phi, .5);
            new_mask = phi<=0;
            c = convergence(prev_mask,new_mask,0,c);
          %  In each  30 iterations obtain another probability density within each of these two regions  can be modeled using a Gaussian probability distribution whose parametersare adaptively updated during the course of evolution of the level set function.
           if ( mod(its,30)==0 ) 
               
                   v = EMLEVELSET(img,phi);
                 
           end 
            if c <= 5
                its = its + 1;
                prev_mask = new_mask;
            else stop = 1;
            end      
        else
            break;
        end
        
    end

    
    
    
    
    
    
    
    
    
    
    %-- final output
imagecompose=affichagesauvegardeimage(img,phi);
imagecompose = uint8( imagecompose );
imagesc(img);colormap(gray);hold on;
[c,h] = contour(phi,[0 0],'r','Linewidth',2);
        



















































 %%%%%%%%%%%%% generation du template %%%%%%%%%%%%%%%%%%    
 img_T = ones(size(phi));
 [n,m]=size(phi);
 for i=1:n
     for j=1:m
         if(phi(i,j)<=0)
             img_T(i,j)=-1;
         end;
     end;
 end;
 %imshow(img_T);
%  save('img_T.txt','img_T','-ascii');
%  save('phi_T.txt','phi','-ascii');
 %imshow(phi)
 %%%%%%%%%%%%% generation du template %%%%%%%%%%%%%%%%%