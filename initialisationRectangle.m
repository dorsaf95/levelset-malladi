function phi= initialisationRectangle(img,x) 

    m = zeros(size(img)); %% initialisation rectangle
    bord=x;
    m(bord:size(img,1)-bord,bord:size(img,2)-bord) = 1;
    m=double(m);
    phi = mask2phi(m);